terraform {
  # Here we configure the providers we need to run our configuration
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.6.0"
    }
  }

  # With this backend configuration we are telling Terraform that the
  # created state should be saved in some Google Cloud Bucket with some prefix
  backend "gcs" {
    ## INSERT YOUR BUCKET HERE!!
    bucket = "binar-devops-terraform-state"
    prefix = "terraform/state"
    credentials = "binar-bootcamp-playground-dcbaafb3527b.json"
  }
}

# We define the "google" provider with the project and the general region + zone
provider "google" {
  credentials = file("binar-bootcamp-playground-dcbaafb3527b.json")
  ## INSERT YOUR PROJECT ID HERE!!
  project = "binar-bootcamp-playground"
  region = "asia-southeast2"
  zone = "asia-southeast2-a"
}


# Enable the Compute Engine API
# Alternatively you can do this directly via the GCP GUI
resource "google_project_service" "compute" {
  ## INSERT YOUR PROJECT ID HERE!!
  project = "binar-bootcamp-playground"
  service = "compute.googleapis.com"
  disable_on_destroy = false
}
# Enable the Cloud Resource Manager API
# Alternatively you can do this directly via the GCP GUI
resource "google_project_service" "cloudresourcemanager" {
  ## INSERT YOUR PROJECT ID HERE!!
  project = "binar-bootcamp-playground"
  service = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "sqladmin" {
  ## INSERT YOUR PROJECT ID HERE!!
  project = "binar-bootcamp-playground"
  service = "sqladmin.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "sql-component" {
  ## INSERT YOUR PROJECT ID HERE!!
  project = "binar-bootcamp-playground"
  service = "sql-component.googleapis.com"
  disable_on_destroy = false
}

# Here we define a very small compute instance
# The actual content of it isn't important its just here for show-case purposes
resource "google_compute_instance" "default" {
  name = "my-vm"
  machine_type = "e2-micro"
  zone = "asia-southeast2-a"

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-2004-lts"
      size = "10"
    }
  }

  network_interface {
    network = "default"
  }

  # Before we can create a compute instance we have to enable the the Compute API
  depends_on = [
    google_project_service.cloudresourcemanager,
    google_project_service.compute]
}
resource "google_sql_database" "database" {
  name     = "my-database"
  instance = google_sql_database_instance.instance.name
}

# See versions at https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/sql_database_instance#database_version
resource "google_sql_database_instance" "instance" {
  name             = "my-database"
  region           = "asia-southeast2"
  database_version = "MYSQL_5_6"
  settings {
    tier = "db-f1-micro"
    disk_type = "PD_SSD"
    disk_size = "10"
  }
    depends_on = [
    google_project_service.sqladmin,
    google_project_service.sql-component]
  deletion_protection  = "true"
}